#include "mdrope.h"
#include <utility>
#include <deque>
#include <cstring>
#include <sstream>

mdrope::mdrope() : mdrope{nullptr, nullptr} {}

mdrope::mdrope(std::shared_ptr<mdrope> left, std::shared_ptr<mdrope> right)
        : left{std::move(left)}, right{std::move(right)}, content{nullptr}, utf8_char_size{0}, byte_size{0} {
    if (this->left) {
        utf8_char_size += this->left->utf8_char_size;
        byte_size = this->left->byte_size;
    }
    if (this->right) {
        utf8_char_size += this->right->utf8_char_size;
        byte_size = this->right->byte_size;
    }
}

mdrope::mdrope(const mdrope &m) : left{m.left}, right{m.right} {
    this->byte_size = m.byte_size;
    this->utf8_char_size = m.utf8_char_size;
    if (m.content) {
        this->content = new char[this->byte_size];
        memcpy(this->content, m.content, byte_size);
    } else {
        this->content = nullptr;
    }
}

mdrope::~mdrope() {
    if (content != nullptr) {
        delete content;
        content = nullptr;
    }
}

mdrope mdrope::parse_mdrope_from_file(const std::string &filename) {
    // TODO : Implement me
    return {};
}

/**
 * Pops an element from the deque and returns it.
 *
 * @attention the content of the deque will change
 *
 * @param deque reference to the deque
 * @return a copied value of the first element in the deque.
 */
static mdrope pop_from_deque(std::deque<mdrope> &deque) {
    mdrope use = deque.front();
    deque.pop_front();
    return use;
}

mdrope mdrope::parse_mdrope_from_string(const std::string &str) {
    std::deque<mdrope> mdropes;
    const char *c_str = str.c_str();
    size_t c_str_iter = 0;

    while (c_str_iter < str.size()) {
        mdropes.push_back(mdrope::parse_mdrope_from_cstring(c_str, c_str_iter));
    }

    while (mdropes.size() > 1) {
        std::deque<mdrope> tmp;

        while (mdropes.size() > 1) {
            auto fst = std::make_shared<mdrope>(pop_from_deque(mdropes));
            auto snd = std::make_shared<mdrope>(pop_from_deque(mdropes));
            tmp.push_back({fst, snd});
        }

        if (mdropes.size() == 1) {
            tmp.push_back(
                    pop_from_deque(mdropes)
            );
        }

        mdropes = tmp;
    }

    return pop_from_deque(mdropes);
}

/**
 * It detects how many bytes does this utf-8 character have
 *
 * @param c the first byte of the character
 * @return number of bytes if it isn't the end of the string, 0 otherwise.
 */
static size_t detect_utf8_bytes(unsigned char c) {
    if (c == '\0')
        return 0; // no need to check anymore
    if ((c & 0x80) == 0x00) // NOLINT
        return 1; // One byte char
    if ((c & 0xE0) == 0xC0) // NOLINT
        return 2; // two byte char
    if ((c & 0xF0) == 0xE0) // NOLINT
        return 3; // three byte char
    if ((c & 0xF8) == 0xF0) // NOLINT
        return 4; // four byte char

    return 0; // None of the above
}

mdrope mdrope::parse_mdrope_from_cstring(const char *str, size_t &pos) {
    mdrope ret{};

    size_t counter = 0;
    size_t utf8_char_counter = 0;
    size_t utf8_bytes = 0;

    while ((utf8_bytes = detect_utf8_bytes((unsigned char) str[pos + counter])) &&
           counter + utf8_bytes <= max_char_per_leaf) {
        counter += utf8_bytes;
        utf8_char_counter++;
    }

    ret.content = new char[counter];
    memcpy(ret.content, str + pos, counter);

    ret.utf8_char_size = utf8_char_counter;
    ret.byte_size = counter;

    pos += counter;

    return ret;
}

mdrope mdrope::parse_mdrope_from_istream(std::istream &is) {
    size_t buffer_size = mdrope::max_char_per_leaf + 4;
    auto *buffer = new char[buffer_size];


    // TODO : Implement me
    return {};
}

std::string mdrope::to_string() const {
    std::ostringstream s;
    s << *this;
    return s.str();
}

mdrope mdrope::add(size_t pos, const std::string &str) const {
    return add(
            pos,
            std::make_shared<mdrope>(mdrope::parse_mdrope_from_string(str))
    );
}

mdrope mdrope::add(size_t pos, std::shared_ptr<mdrope> rope) const {
    if (pos == 0) {
        // Add at the beginning
        auto self_copy = mdrope{this->left, this->right};
        return {
                rope,
                std::make_shared<mdrope>(self_copy)
        };
    }
    if (pos == this->size()) {
        // Add at the end
        auto self_copy = mdrope{this->left, this->right};
        return {
                std::make_shared<mdrope>(self_copy),
                rope
        };
    }

    auto next_split = this->split(pos);
    if (next_split.first.size() > next_split.second.size()) {
        // concat with the second one first, and then both with the first one
        auto use = mdrope{rope, std::make_shared<mdrope>(next_split.second)};
        return {
                std::make_shared<mdrope>(next_split.first),
                std::make_shared<mdrope>(use)
        };
    }
    // concat with the first one first, and then both with the second one
    auto use = mdrope{std::make_shared<mdrope>(next_split.first), rope};
    return {
            std::make_shared<mdrope>(use),
            std::make_shared<mdrope>(next_split.second)
    };
}

mdrope mdrope::remove(size_t from, size_t n) const {
    auto next_split = this->split(from);

    auto fst = next_split.first;
    auto snd = next_split.second.split(n).second;

    return {
            std::make_shared<mdrope>(fst),
            std::make_shared<mdrope>(snd)
    };
}

mdrope mdrope::concat(std::shared_ptr<mdrope> fst, std::shared_ptr<mdrope> snd) {
    return {std::move(fst), std::move(snd)};
}

/**
 * It splits a character array into two strings. The parameters need to be corrent so that there
 * is no memory errors.
 * The position is the number of utf-8 characters and not the bytes.
 *
 * @param str the character array
 * @param str_size number of bytes
 * @param pos the position where the split should happen
 * @return a pair with two std::string elements, each holding the given part.
 */
static std::pair<std::string, std::string> split_utf_string(const char *str, size_t str_size, size_t pos) {
    size_t byte_counter = 0;
    size_t utf_counter = 0;
    auto it = (unsigned char *) str;

    while (utf_counter != pos) {
        auto changes = detect_utf8_bytes(*it);
        byte_counter += changes;
        it += changes;
        utf_counter++;
    }
    std::ostringstream ret_left;
    std::ostringstream ret_right;

    ret_left.write(str, byte_counter);
    ret_right.write(str + byte_counter, str_size - byte_counter);

    return {ret_left.str(), ret_right.str()};
}

std::pair<mdrope, mdrope> mdrope::split(size_t pos) const {
    if (pos >= this->utf8_char_size || pos == 0)
        throw std::runtime_error("mdrope::split : the position must be somewhere in the middle of the rope");

    if (this->content != nullptr) {
        // IF THIS IS A LEAF
        auto next_split = split_utf_string(this->content, this->byte_size, pos);
        return {
                mdrope::parse_mdrope_from_string(next_split.first),
                mdrope::parse_mdrope_from_string(next_split.second)
        };
    }


    // IF THIS IS NOT A LEAF
    auto left_char_size = (this->left)->utf8_char_size;

    if (pos == left_char_size) {
        return {*(this->left), *(this->right)};
    }

    if (pos > left_char_size) {
        auto next_split = this->right->split(pos - left_char_size);
        return {
                {this->left, std::make_shared<mdrope>(next_split.first)},
                next_split.second
        };
    }

    auto next_split = this->left->split(pos);
    return {
            next_split.first,
            {std::make_shared<mdrope>(next_split.second), this->right}
    };
}


std::ostream &operator<<(std::ostream &o, const mdrope &m) {
    if (m.left != nullptr) {
        o << *m.left;
    }

    if (m.content) {
        o.write(m.content, m.byte_size);
    }

    if (m.right != nullptr) {
        o << *m.right;
    }

    return o;
}

size_t mdrope::size() const {
    return this->utf8_char_size;
}

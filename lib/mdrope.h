/**
 * @author Dardan (email: mail 'at' dardan 'dot' im)
 * @short Implementation of rope data structure in cpp
 *
 * This is an implementation of the rope data structure. At first it was intended to hold
 * Markdown text, but during the implementation it changed to work with utf-8 plain text.
 *
 * The goal of this project is to practice my c++ skills. I also thought programming some
 * algorithms and data structures would also help me in general.
 *
 * The code is licensed under MIT License.
 */
#ifndef CPP_ROPES_MDROPE_H
#define CPP_ROPES_MDROPE_H

#include <iostream>
#include <string>
#include <memory>

struct mdrope {
private:
    std::shared_ptr<mdrope> left;
    std::shared_ptr<mdrope> right;
    char *content;
    size_t utf8_char_size;
    size_t byte_size;
    static const size_t max_char_per_leaf{12};

    /**
     * The constructor with two mdrope parameters.
     * Is used to create a new rope with left and right as child nodes.
     *
     * @param left
     * @param right
     */
    mdrope(std::shared_ptr<mdrope> left, std::shared_ptr<mdrope> right);

    /**
     * Given an istream, it creates a rope with the same content.
     *
     * @param is the given istream
     * @return the constructed rope
     */
    static mdrope parse_mdrope_from_istream(std::istream &is);

    /**
     * Given a cstring and a position, this function creates a leaf with n bytes, where n is smaller or equals to max_char_per_leaf.
     * It changes the pos parameter accordingly and returns a new mdrope.
     *
     * This is intended for internal use only
     *
     * @param str the cstring to read from
     * @param pos the start position of where to read from
     * @return the mdrope leaf with the string content
     */
    static mdrope parse_mdrope_from_cstring(const char *str, size_t &pos);

public:
    /**
     * Default constructor. It creates an empty rope. It's useless at the time so it might be private in the future.
     */
    mdrope();

    /**
     * The copy constructor.
     * Makes sure to copy the content so that the changes don't affect previous ropes
     *
     * @param m the rope to copy
     */
    mdrope(const mdrope &m);

    /**
     * The destructor
     */
    ~mdrope();

    /**
     * Given a file, it creates a rope with the same content.
     *
     * @param filename the location of the file
     * @return the constructed rope
     */
    static mdrope parse_mdrope_from_file(const std::string &filename);

    /**
     * Given a string, it creates a rope with the same content.
     *
     * @param str the string input
     * @return the constructed rope
     */
    static mdrope parse_mdrope_from_string(const std::string &str);

    /**
     * Creates a string with the content of the rope
     *
     * @return the constructed string
     */
    std::string to_string() const;

    /**
     * Adds a string at the given position and returns the newly created rope.
     *
     * @param pos the position where the string needs to be added
     * @param str the string to be added
     * @return the new rope that includes the given string
     */
    mdrope add(size_t pos, const std::string &str) const;

    /**
     * Adds a rope at the given position and returns the newly created rope.
     *
     * @param pos the position where the rope needs to be added
     * @param rope the rope to be added
     * @return the new rope that includes the given rope
     */
    mdrope add(size_t pos, std::shared_ptr<mdrope> rope) const;

    /**
     * Removes the content between position *from* until position *from + n*
     *
     * @param from position of the start of removal
     * @param n number of characters to remove
     * @return the rope after the removal of those characters.
     */
    mdrope remove(size_t from, size_t n) const;

    /**
     * Concatinates two ropes together.
     *
     * @param fst left side of the rope
     * @param snd right side of the rope
     * @return the newly constructed rope
     */
    static mdrope concat(std::shared_ptr<mdrope> fst, std::shared_ptr<mdrope> snd);

    /**
     * Splits the rope in two parts. The left size will have exactly *pos* elements, and the
     * right side will have the rest of the characters.
     *
     * @param pos the position where the rope is going to be split
     * @return the constructed rope
     */
    std::pair<mdrope, mdrope> split(size_t pos) const;

    /**
     * Writes the content of the rope to an ostream
     *
     * @param o the given ostream to write to
     * @param m the rope
     * @return the same ostream
     */
    friend std::ostream &operator<<(std::ostream &o, const mdrope &m);

    /**
     * @return The number of UTF-8 characters in the rope.
     */
    size_t size() const;
};

#endif //CPP_ROPES_MDROPE_H

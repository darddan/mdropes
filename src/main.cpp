#include <iostream>
#include "mdrope.h"

int main() {
    std::shared_ptr<mdrope> first = std::make_shared<mdrope>(mdrope::parse_mdrope_from_string("Hello, I'm Dardan! "));
    std::cout << *first << std::endl;

    auto second = std::make_shared<mdrope>(mdrope::parse_mdrope_from_string("I'm trying to implement ropes!"));
    std::cout << second->to_string() << std::endl;

    mdrope third = mdrope::concat(first, second);
    std::cout << third << std::endl;

    auto fourth = third.split(first->size());

    std::cout << fourth.first << std::endl;
    std::cout << fourth.second << std::endl;

    auto fifth = third.split(7);
    std::cout << fifth.first << std::endl;
    std::cout << fifth.second << std::endl;

    auto sixth = third.add(7, "how are you doing? ");
    std::cout << sixth << std::endl;

    auto seventh = third.remove(11, 6).add(11, "Someone Else");
    std::cout << seventh << std::endl;

    std::cout << "EOP!" << std::endl;
    return 0;
}

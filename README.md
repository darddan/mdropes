# mdrope

### A rope implementation in c++

Because there are a lot of rope implementations in different languages (including c++), there is no need at all for this one. Practice was the main purpose of this library.

It started as a data structure to hold Markdown content (hence the name), but during the implementation I found out that didn't make any sense so I switched it to work with utf-8 text.

## Goals

* Use modern c++
* Safety 
* Efficiency
* Fast text search
* Self balanced

## State of the code

The code is not usable at this time. Since there is no tests yet, there are probably a lot of bugs there.
I just implemented some methods so that they work but none of the goals are achieved. A lot of other functions are missing.


## Contribute

There are multiple ways to contribute:

* Open an issue on GitLab repo
* Solve an existing issue
* Write/Improve documentation (the current one is confusing and has grammatical errors)
* Fix bugs directly and make a pull request

#### Git repo

I will try to push to GitHub every time I merge something to master but I will mainly work on GitLab

* [GitLab](https://gitlab.com/darddan/mdropes)
* [GitHub](https://github.com/darddan/mdropes)

#### Rules

Try to be consistent with git commit messages and try to document (using comments) the code that you write.
I work with CLion and I try to reformat the code using their default formatter/style (Linux & Windows: Ctrl + Alt + L)
